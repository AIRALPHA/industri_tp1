var storedUsers = [];
var user_id = 0;
var i = 0;

function editUsers(i) {
  var storedUsers = JSON.parse(localStorage.getItem('userData'));
  $("#editForm").find('input[name="name"]').val(storedUsers[i]["name"]);
  $("#editForm").find('input[name="prenom"]').val(storedUsers[i]["prenom"]);
  $("#editForm").find('input[name="id"]').val(i);
}


function deleteUsers(i) {
  var storedUsers = JSON.parse(localStorage.getItem('userData'));
  storedUsers.splice(i, 1);
  if (storedUsers.length == 0)
    $('table').remove();
  window.localStorage.setItem('userData', JSON.stringify(storedUsers));
  window.location.reload();
}


$(document).ready(function () {
  if (localStorage.getItem("userData") !== null) {
    storedUsers = JSON.parse(localStorage.getItem("userData"));

    for (i = 0; i < storedUsers.length; i++) {
      var id = i + 1;
      $('tbody').append('<tr><th scope="row">' + id + '</th><td>' + storedUsers[i]['name'] + '</td><td>' + storedUsers[i]['prenom'] + '</td><td> <button type=\"submit\" class=\"btn btn-success\" onclick=\"editUsers(' + i + ')\" data-bs-toggle=\"modal\" data-bs-target=\"#editModal\">Edit</button>|<button type=\"submit\" class=\"btn btn-danger\" onclick=\"deleteUsers(' + i + ')\">Delete</button></td>' + '</tr>');
    }
  }
});


$('#inputForm').on('submit', function (e) {
  e.preventDefault();

  if (localStorage.getItem("userData") !== null) {
    storedUsers = JSON.parse(localStorage.getItem("userData"));
  }

  if (localStorage.getItem("userData") === null) {
    $('table').append('<tr><th> Name </th><th> Email </th></tr>');
  }

  var n = $('#inputForm').find('input[name="name"]').val();

  var p = $('#inputForm').find('input[name="prenom"]').val();

  const user = {
    name: n,
    prenom: p,
  }

  storedUsers.push(user);

  window.localStorage.setItem('userData', JSON.stringify(storedUsers));

  $(this).closest('form').find('input[name="name"]').val("");
  $(this).closest('form').find('input[name="prenom"]').val("");
  window.location.reload();
});

$('#editForm').on('submit', function (e) {
  e.preventDefault();

  storedUsers = JSON.parse(localStorage.getItem("userData"));

  var i = $('#editForm').find('input[name="id"]').val();

  var n = $('#editForm').find('input[name="name"]').val();

  var p = $('#editForm').find('input[name="prenom"]').val();

  console.log(i);
  storedUsers[i]["name"] = n;
  storedUsers[i]["prenom"] = p;

  window.localStorage.setItem('userData', JSON.stringify(storedUsers));

  window.location.reload();

});
